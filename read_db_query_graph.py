from pprint import pprint
import networkx as nx
import csv

fields = ('sql_id', 'module_count', 'modules', 'tables', 'tables_with_modules',
          'table_counts_per_module', 'module_tables', 'sql', 'long_sql')

tables = set()
query_tables = set()

with open("db_object_usage.csv", newline='') as f:
    reader = csv.DictReader(f, fieldnames=fields, delimiter=',', quotechar='"')

    for row in reader:
        if len(row) != 9:
            continue

        tables_str = row['tables']
        if tables_str is None:
            continue

        query_tables.add(tables_str)
        tables.update(tables_str.split(','))


pprint(query_tables)
pprint(tables)

table_graph = nx.Graph()
table_graph.add_nodes_from(tables)

for tables_str in query_tables:
    linked_tables = tables_str.split(',')
    for i in range(len(linked_tables)):
        for j in range(i+1, len(linked_tables)):
            table_graph.add_edge(linked_tables[i], linked_tables[j])


