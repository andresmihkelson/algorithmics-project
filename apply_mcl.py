from read_db_query_graph import table_graph
from read_db_objects import db_objects

import networkx as nx
import networkx.exception as nx_exception
import markov_clustering as mc

from matplotlib.pyplot import rcParams
from matplotlib.pylab import show, cm, axis

from pprint import pprint

rcParams['figure.figsize'] = 30, 30

# Finding tables to remove
tables_to_remove = filter(lambda o: o.module in ('SYSTEM', 'ADMIN'), db_objects)
tables_to_remove = list(map(lambda t: t.name, tables_to_remove))

# Removing tables form specific base modules
for table in tables_to_remove:
    try:
        table_graph.remove_node(table)
    except nx_exception.NetworkXError:
        pass

# Find nodes with degrees and sort them
degrees = list(table_graph.degree())
degrees.sort(key=lambda d: d[1], reverse=True)

# Removing most often used tables (common ones that have to be denormalised for microservices)
for table, degree in degrees[:16]:
    table_graph.remove_node(table)

# Build a map for later use
degrees_map = dict(degrees)

# MCL preparations
sparse_adj_mat = nx.to_scipy_sparse_matrix(table_graph)

# Get clusters with MCL
mcl = mc.run_mcl(sparse_adj_mat, expansion=2, inflation=2)
clusters = mc.get_clusters(mcl)

# Map clusters to colors
cluster_map = {node: i for i, cluster in enumerate(clusters) for node in cluster}
colors = []

for i, node in enumerate(table_graph.nodes()):
    color_id = cluster_map[i]
    colors.append(color_id)
    table_graph.node[node]['color'] = color_id

# Create visuals
nx.draw_networkx(table_graph, node_color=colors, cmap=cm.tab20, node_size=20, with_labels=False, edge_color="silver")
axis("off")
show(block=False)

# Show text output
tables = list(table_graph.nodes())
for i, cluster in enumerate(clusters):
    l = list(map(lambda node_nr: tables[node_nr] + " [" + str(degrees_map[tables[node_nr]]) + "]", cluster))
    print("{:>2} {}".format(i, ", ".join(l)))

# Output graph to GEXF
nx.write_gexf(table_graph, "out.gexf")
