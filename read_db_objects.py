from pprint import pprint
import csv


class DbObject:
    def __init__(self, name, module):
        self.name = name
        self.module = module

    def __repr__(self):
        return "%s [%s]" % (self.name, self.module)


db_objects = []

with open("db_objects.csv", newline='') as f:
    reader = csv.reader(f, delimiter=',', quotechar='"')

    for row in reader:
        if len(row) != 5:
            continue
        if row[2] != 'TABLE':
            continue

        db_objects.append(DbObject(row[1], row[0]))

db_objects.sort(key=lambda dbo: dbo.module)
